﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class QuizTest : MonoBehaviour {

    public ReadCSV _RefReadCSV;

    public GameObject _RegisterPanel;
    public InputField _InputName;
    public InputField _InputMobNumber;

    public GameObject _AfterLoginPanel;


    public GameObject _QuestionPanel;
    public Text _TextName;

    public ToggleGroup _AnswerGroup;
    public Toggle _ToggleOptionA;
    public Toggle _ToggleOptionB;
    public Toggle _ToggleOptionC;
    public Toggle _ToggleOptionD;
    public Text _TextOptionA;
    public Text _TextOptionB;
    public Text _TextOptionC;
    public Text _TextOptionD;

    public Text _TextQuestionNum;

    public Text _TextQuestion;
    public RawImage _ImageQuestion;

    public GameObject _ResultPanel;
   // public Text _TextCongrats;
  //  public Text _TextScore;

    public GameObject _ResultLosePanel;

    int MaxQuestion = 10;
    int score = 0;

    public List<int> mListUniqQuestion = new List<int>();

   public List<string> mListAnswerScore = new List<string>();


    // Use this for initialization
    void Start () {

        ShowRegisterPanel();

        for (int i = 0; i < 10;i++){
            mListAnswerScore.Add("0");
        }

    }
    public Text _TextTime;
    float mTimeSpend = 180f;
    TimeSpan t;
    bool isTimeStop = false;
    // Update is called once per frame
    void Update () {


        if(isTimeStop){
            return;
        }
        if (mTimeSpend >= 0)
        {
            mTimeSpend -= Time.deltaTime;
            t = TimeSpan.FromSeconds((int)mTimeSpend);

            _TextTime.text = t.ToString();

        }else
        {
            isTimeStop = true;
            //goto result screen
            ShowResultPanel();
        }
    }

    public void ShowAfterLoginPanel(){



        if (string.IsNullOrEmpty(_InputName.text) || string.IsNullOrEmpty(_InputMobNumber.text))
        {
            return;
        
        } 
        else
        {

            for (int i = 0; i < _RefReadCSV._ListMobileNumber_2ndTry.Count; i++)
            {
                if(string.CompareOrdinal(_InputMobNumber.text , _RefReadCSV._ListMobileNumber[i]) == 0){
                    _InputMobNumber.text = "";
                    _InputMobNumber.placeholder.GetComponent<Text>().text = "You have exhausted your maximum chances.";
                    return;
                }
            }

            // for (int i = 0; i < _RefReadCSV._ListMobileNumber.Count; i++)
            // {
            //     if(string.CompareOrdinal(_InputMobNumber.text , _RefReadCSV._ListMobileNumber[i]) == 0){

            //         _InputMobNumber.text = "";
            //         _InputMobNumber.placeholder.GetComponent<Text>().text = "Already Mobile Number Registerd";
            //         return;
            //     }
            // }

            
        }

        if (_InputMobNumber.text.Length == 10)
        {
            _RegisterPanel.SetActive(false);
            _AfterLoginPanel.SetActive(true);
            StartCoroutine(CloseAfterLoginPanel());
        }else{
            _InputMobNumber.text = "";
            _InputMobNumber.placeholder.GetComponent<Text>().text = "Enter Correct Mobile Number";
            Debug.Log("Enter Correct MobileNumber");
        }
    }

    IEnumerator CloseAfterLoginPanel(){

        yield return new WaitForSeconds(5);
        ShowQuestionPanel();

        float alphaP = 1;
        while (alphaP > 0)
        {
            yield return new WaitForSeconds(0.1f);
            alphaP -= 0.1f;
            _AfterLoginPanel.GetComponent<CanvasGroup>().alpha = alphaP;
        }

        _AfterLoginPanel.SetActive(false);
        _AfterLoginPanel.GetComponent<CanvasGroup>().alpha = 1;

    }

    public void ShowQuestionPanel()
    {
       
            score = 0;
            QuestionCount =-1;
            QuestionIndex = 0;
            mTimeSpend = 180;
            isTimeStop = false;
            _TextName.text = "Hi " + _InputName.text + "!";

            for (int i = 0; i < 10; i++)
            {
                mListAnswerScore[i] = "0";
            }
            mListUniqQuestion.Clear();
            _RegisterPanel.SetActive(false);
            _QuestionPanel.SetActive(true);
            FillQuestionIndex();
            GoNext();

       

    }

    int QuestionIndex;

    int QuestionCount;

    void SetIntractableToggle(bool isIntactable){

        _ToggleOptionA.interactable = isIntactable;
        _ToggleOptionB.interactable = isIntactable;
        _ToggleOptionC.interactable = isIntactable;
        _ToggleOptionD.interactable = isIntactable;

    }


    public GameObject _NextButton;
    public GameObject _BackButton;

    public void  GoNext(){

        QuestionCount++;
       
        if(QuestionCount>0){
            _BackButton.SetActive(true);
        }else{
            _BackButton.SetActive(false);
        }
        StartCoroutine(NextQuestion(false));
    }

    public void GoBack()
    {

        QuestionCount--;
        if (QuestionCount == 0)
        {
            _BackButton.SetActive(false);
        }
       
        StartCoroutine(NextQuestion(false));

    }

    public IEnumerator NextQuestion(bool iswait = true){

        Debug.Log("QuestionCount" +QuestionCount);
        if (iswait)
            yield return new WaitForSeconds(1);
        else
            yield return null;
            
        if (QuestionCount >= MaxQuestion){
         //   mTimeSpend = 180f;
            ShowResultPanel();
        }else{
           
         
            QuestionIndex = mListUniqQuestion[QuestionCount];

            _AnswerGroup.SetAllTogglesOff();

            _TextQuestionNum.text = QuestionCount.ToString();

            
            if (!string.IsNullOrEmpty(_RefReadCSV._ListQuiz[QuestionIndex]._ImageName))
            {
                _ImageQuestion.gameObject.SetActive(true);
                _ImageQuestion.texture = ReadCSV.LoadPNG(_RefReadCSV._ListQuiz[QuestionIndex]._ImageName);
              // _ImageQuestion.SetNativeSize();
            }
            else
            {
                _ImageQuestion.gameObject.SetActive(false);
            }
            _TextQuestion.text = _RefReadCSV._ListQuiz[QuestionIndex]._Question;
            _TextOptionA.text =  _RefReadCSV._ListQuiz[QuestionIndex]._OptionA;
            _TextOptionB.text =  _RefReadCSV._ListQuiz[QuestionIndex]._OptionB;
            _TextOptionC.text =  _RefReadCSV._ListQuiz[QuestionIndex]._OptionC;
            _TextOptionD.text =  _RefReadCSV._ListQuiz[QuestionIndex]._OptionD;

            if(mListAnswerScore[QuestionCount] == "A"){
                _ToggleOptionA.isOn = true;
            }else if (mListAnswerScore[QuestionCount] == "B")
            {
                _ToggleOptionB.isOn = true;
            }
            else if (mListAnswerScore[QuestionCount] == "C")
            {
                _ToggleOptionC.isOn = true;
            }
            else if (mListAnswerScore[QuestionCount] == "D")
            {
                _ToggleOptionD.isOn = true;
            }


        }


    }
    public List<int> listAskedQuestion = new List<int>();

    void FillQuestionIndex(){


        int randomNo = 0;

        for (int k = 0; k < MaxQuestion;k++){
            if (listAskedQuestion.Count >= _RefReadCSV._ListQuiz.Count)
            {
                listAskedQuestion.RemoveRange(0, _RefReadCSV._ListQuiz.Count - k);
            }

            for (int j = 0; j < 1; j++)
            {
                randomNo = UnityEngine.Random.Range(0, _RefReadCSV._ListQuiz.Count);
                for (int i = 0; i < listAskedQuestion.Count; i++)
                {
                    if (listAskedQuestion[i] == randomNo)
                    {
                        j--;
                        break;
                    }

                }

            }
            listAskedQuestion.Add(randomNo);
            mListUniqQuestion.Add(randomNo);
        }
      
    }
    public void OptionAChange(){
        if (_RefReadCSV._ListQuiz[QuestionIndex]._Answer == "A" && _ToggleOptionA.isOn)
        {
            Debug.Log("Correct Ans");
        }


        if (_ToggleOptionA.isOn){
            _TextOptionA.color = Color.white;
            _TextOptionA.text = _RefReadCSV._ListQuiz[QuestionIndex]._OptionA;
            mListAnswerScore[QuestionCount] = "A";


        }
        else
        {
            _TextOptionA.supportRichText = true;
            _TextOptionA.color = Color.black;
            _TextOptionA.text = _RefReadCSV._ListQuiz[QuestionIndex]._OptionA;


        }
    }
    public void OptionBChange()
    {
        if (_RefReadCSV._ListQuiz[QuestionIndex]._Answer == "B" && _ToggleOptionB.isOn)
        {
            Debug.Log("Correct Ans");
            score++;
        }
       
        if (_ToggleOptionB.isOn)
        {
            _TextOptionB.color = Color.white;
            _TextOptionB.text =  _RefReadCSV._ListQuiz[QuestionIndex]._OptionB;
            mListAnswerScore[QuestionCount] = "B";

        }
        else
        {
            _TextOptionB.supportRichText = true;
            _TextOptionB.color = Color.black;
            _TextOptionB.text =   _RefReadCSV._ListQuiz[QuestionIndex]._OptionB;

        }
    }
    public void OptionCChange()
    {
        if (_RefReadCSV._ListQuiz[QuestionIndex]._Answer == "C" && _ToggleOptionC.isOn)
        {
            Debug.Log("Correct Ans");
            score++;
        }
       
        if (_ToggleOptionC.isOn)
        {
            _TextOptionC.color = Color.white;
            _TextOptionC.text = _RefReadCSV._ListQuiz[QuestionIndex]._OptionC;
            mListAnswerScore[QuestionCount] = "C";

        }
        else
        {
            _TextOptionC.supportRichText = true;
            _TextOptionC.color = Color.black;
            _TextOptionC.text = _RefReadCSV._ListQuiz[QuestionIndex]._OptionC;
        }
    }
    public void OptionDChange()
    {
        if (_RefReadCSV._ListQuiz[QuestionIndex]._Answer == "D" && _ToggleOptionD.isOn)
        {
            Debug.Log("Correct Ans");
            score++;
        }
       
        if (_ToggleOptionD.isOn)
        {
            _TextOptionD.color = Color.white;
            _TextOptionD.text = _RefReadCSV._ListQuiz[QuestionIndex]._OptionD;
            mListAnswerScore[QuestionCount] = "D";

        }
        else
        {
            _TextOptionD.supportRichText = true;
            _TextOptionD.color = Color.black;
            _TextOptionD.text =  _RefReadCSV._ListQuiz[QuestionIndex]._OptionD;
        }
    }

    public void ShowResultPanel(){

        isTimeStop = true;

        //calulate Score 
        score = 0;
        for (int i = 0; i < MaxQuestion; i++){

            if( string.CompareOrdinal(_RefReadCSV._ListQuiz[mListUniqQuestion[i]]._Answer, mListAnswerScore[i] ) == 0 ){
                score++;
            }
        }
        if(score>6){
            //win screen
            _ResultPanel.SetActive(true);

        }
        else
        {
            //lose screen
            _ResultLosePanel.SetActive(true);


        }

//        _TextCongrats.text = "Congratulation " + _InputName.text + "!";
  //      _TextScore.text = score.ToString() + "/" + MaxQuestion.ToString();
    //    _RefReadCSV.AddScore(_InputName.text,_InputMobNumber.text, MaxQuestion.ToString(), score.ToString(), 180 - mTimeSpend );
        _ResultPanel.SetActive(true);


        bool isFirstChance = true;
        for (int i = 0; i < _RefReadCSV._ListMobileNumber.Count; i++)
        {
            if(string.CompareOrdinal(_InputMobNumber.text , _RefReadCSV._ListMobileNumber[i]) == 0)
            {                
                isFirstChance = false;
                break;
            }
        }
        _RefReadCSV.AddScore(isFirstChance,_InputName.text,_InputMobNumber.text, MaxQuestion.ToString(), score.ToString(), 180-mTimeSpend );

        StartCoroutine(WaitforRegisterPanel());
    }


    IEnumerator WaitforRegisterPanel(){
        yield return new WaitForSeconds(5f);
        ShowRegisterPanel();


    }
    public void ShowRegisterPanel()
    {

        _ResultPanel.SetActive(false);
        _ResultLosePanel.SetActive(false);

        _QuestionPanel.SetActive(false);
        _AfterLoginPanel.SetActive(false);

        _RegisterPanel.SetActive(true);

        _InputName.text = "";
        _InputMobNumber.text = "";
        _InputName.placeholder.GetComponent<Text>().text = "";
        _InputMobNumber.placeholder.GetComponent<Text>().text = "";
    }


  

}
