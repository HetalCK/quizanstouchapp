﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using System.Text;
public class ReadCSV : MonoBehaviour
{

    static string m_Path;
    public Text _Text;
    public RawImage _RawImage;

    public List<Quiz> _ListQuiz = new List<Quiz>();

    // Use this for initialization
    void Start()
    {

        //Get the path of the Game data folder
        m_Path = Application.dataPath + "/Data/";

        //Output the Game data path to the console
        Debug.Log("Path : " + m_Path);

        ReadCSVFile();
        CreateStoreData();
        //  _RawImage.texture = LoadPNG("‎⁨iCloud Drie⁩ /Desktop/Data/Picture2.png");
    }

    // Update is called once per frame
    void ReadCSVFile()
    {
        StreamReader strReader = new StreamReader(m_Path + "Quiz.csv");

        bool endofFile = false;
        int index = 0;

        while (!endofFile)
        {
            string data_string = strReader.ReadLine();
            if (data_string == null)
            {
                endofFile = true;
                break;
            }
            var data_vales = data_string.Split(',');
            // Debug.Log(data_vales[0].ToString() + " " + data_vales[1].ToString() + " " + data_vales[2].ToString());
            if (index > 0)
            {
                Quiz quiz = new Quiz();
                quiz._Index = data_vales[0].ToString();
                quiz._ImageName = Between(quiz._Question = data_vales[1].ToString());

                if (quiz._ImageName != null)
                {
                    string s = data_vales[1].ToString();
                    s = s.Replace("#" + quiz._ImageName + ")", "");
                    s = s.Replace(";" , ",");
                    quiz._Question = s;
                }
                else
                {
                    quiz._Question = data_vales[1].ToString();
                }

                string ans = data_vales[2].ToString();
                ans = ans.Replace(";", ",");
                quiz._OptionA = ans;

                 ans = data_vales[3].ToString();
                ans = ans.Replace(";", ",");
                quiz._OptionB = ans;

                ans = data_vales[4].ToString();
                ans = ans.Replace(";", ",");
                quiz._OptionC = ans;

                ans = data_vales[5].ToString();
                ans = ans.Replace(";", ",");
                quiz._OptionD = ans;

                quiz._Answer = data_vales[6].ToString();


                _ListQuiz.Add(quiz);
            }


            _Text.text = data_vales[0];
            index++;
        }
    }

    [System.Serializable]
    public class Quiz
    {

        public string _Index;
        public string _Question;
        public string _OptionA;
        public string _OptionB;
        public string _OptionC;
        public string _OptionD;
        public string _Answer;

        public string _ImageName;

    }
    public string Between(string STR)
    {
        char startDelimiter = '#';
        char endDelimiter = ')';

        bool collect = false;

        string parsedString = "";

        foreach (char c in STR)
        {
            if (c == startDelimiter)
                collect = true;

            if (c == endDelimiter)
                collect = false;

            if (collect == true && c != startDelimiter)
                parsedString += c;
        }
        return parsedString;

    }
    public static Texture2D LoadPNG(string filePath)
    {

        Texture2D tex = null;
        byte[] fileData;

        Debug.Log(filePath);
        if (File.Exists(m_Path + filePath))
        {
            fileData = File.ReadAllBytes(m_Path + filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        }
        return tex;
    }


    #region StoreData
    static string csvpath = "ScoreData.csv";
    static string csvpath_2nd_chance = "ScoreData_2nd_chance.csv";
    static string defaultPath = "";
    public List<string> _ListMobileNumber = new List<string>();
    public List<string> _ListMobileNumber_2ndTry = new List<string>();


    void CreateStoreData()
    {
        defaultPath = Application.dataPath;
        if (!File.Exists(m_Path + csvpath))
        {
            StringBuilder csvcontent = new StringBuilder();

            csvcontent.AppendLine("Name,MobileNumber,Question,Score1,SpendTime");
            // csvcontent.AppendLine("Hetal,987654321,10,8,120sec");
            System.IO.File.AppendAllText(m_Path + csvpath, csvcontent.ToString());


        }
        else
        {

            FillMobileNumList(true);
        }

        if (!File.Exists(m_Path + csvpath_2nd_chance))
        {
            StringBuilder csvcontent = new StringBuilder();
            csvcontent.AppendLine("Name,MobileNumber,Question,Score1,SpendTime");
            // csvcontent.AppendLine("Hetal,987654321,10,8,120sec");
            System.IO.File.AppendAllText(m_Path + csvpath_2nd_chance, csvcontent.ToString());


        }
        else
        {

            FillMobileNumList(false);
        }
    }

    public void AddScore(bool isFirstChance, string aName, string aMobNum, string aQue, string aScore, float atimeSpend)
    {

        Debug.Log("Added Score");
        if (isFirstChance)
            _ListMobileNumber.Add(aMobNum);
        else
            _ListMobileNumber_2ndTry.Add(aMobNum);

        StringBuilder csvcontent = new StringBuilder();
        csvcontent.AppendLine(aName + "," + aMobNum + "," + aQue + "," + aScore + "," + ((int)atimeSpend).ToString());


        System.IO.File.AppendAllText(m_Path + (isFirstChance ? csvpath : csvpath_2nd_chance), csvcontent.ToString());
        Debug.Log(System.IO.File.GetAttributes(m_Path + (isFirstChance ? csvpath : csvpath_2nd_chance)));
        //FileAttributes attributes = 
        // System.IO.File.SetAttributes(m_Path + csvpath,)
    }

    void FillMobileNumList(bool isFirstChance)
    {

        StreamReader strReader = new StreamReader(m_Path + (isFirstChance ? csvpath : csvpath_2nd_chance));
        if (isFirstChance)
            _ListMobileNumber.Clear();
        else
            _ListMobileNumber_2ndTry.Clear();

        bool endofFile = false;
        int index = 0;

        while (!endofFile)
        {
            string data_string = strReader.ReadLine();
            if (data_string == null)
            {
                endofFile = true;
                break;
            }
            var data_vales = data_string.Split(',');
            // Debug.Log(data_vales[0].ToString() + " " + data_vales[1].ToString() + " " + data_vales[2].ToString());
            if (index > 0)
            {
                if (isFirstChance)
                    _ListMobileNumber.Add(data_vales[1]);
                else
                    _ListMobileNumber_2ndTry.Add(data_vales[1]);
            }

            index++;
        }
    }

    //void AddLines()
    //{
    //string[] lines = System.IO.File.ReadAllLines(m_Path + csvpath);
    //string allString = "";
    //if (lines[0] != null) {
    //    lines[0] = "lol";
    //}
    //for(int i=0; i<lines.Length; i++) {
    //    allString += lines[i]+"\n";
    //}                    System.IO.File.WriteAllText("F:/PlayerSets.txt", allString);
    // }

#endregion
}
